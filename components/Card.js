import Link from 'next/link';

export default function Card({ title, content, id, handleDeletePost }) {
	return (
		<div className='card otoklix__postCard otoklix__mb20'>
			<Link href={`post/${id}`}>
				<div className='otoklix__mb10 otoklix__pointer otoklix__overflowHidden otoklix__height100Percent'>
					<div className='card-body otoklix__width90Percent'>
						<h5 className='card-title'>{title}</h5>
						<p className='card-text otoklix__overflowHidden'>{content}</p>
					</div>
				</div>
			</Link>
			<div className='otoklix__flex otoklix__flexAround otoklix__flexRow otoklix__mtAuto'>
				<Link href={`/update/${id}`}>
					<div className='otoklix__editDelete otoklix__pointer'>Edit</div>
				</Link>
				<div className='otoklix__editDelete otoklix__pointer' onClick={() => handleDeletePost(id, title, content)}>
					Delete
				</div>
			</div>
		</div>
	);
}
