import { useEffect, useState } from 'react';
import Navigation from '../../components/Navigation';
import Head from 'next/head';
import axios from 'axios';
import { useRouter } from 'next/router';

export default function post({ article, loading }) {
	console.log(article, 'paths');
	const [chosenMenu, setChosenMenu] = useState('posts');

	const handleClickMenu = (value) => {
		if (value) {
			setChosenMenu(value);
			console.log('hitt', value);
		}
	};

	return (
		<Navigation chosenMenu={chosenMenu} handleClickMenu={handleClickMenu}>
			{loading ? (
				<div className='otoklix__loadingCenter' />
			) : (
				<>
					<Head>
						<title>{article.title}</title>
						<meta name='viewport' content='initial-scale=1.0, width=device-width' />
					</Head>
					<div className='otoklix__wordBreak'>
						<h1 className='otoklix__mb30'>{article.title}</h1>
						<p className='otoklix__pr20'>{article.content}</p>
					</div>
				</>
			)}
		</Navigation>
	);
}
export const getStaticPaths = async () => {
	const res = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}posts`);
	const data = await res.json();

	const paths = data.map((post) => {
		return {
			params: { id: post.id.toString() },
		};
	});

	return {
		paths,
		fallback: false, // false or 'blocking'
	};
};

export const getStaticProps = async (context) => {
	const id = context.params.id;
	const res = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}posts/` + id);
	const data = await res.json();

	return {
		props: { article: data, loading: false },
	};
};
