import { useEffect, useState } from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import Navigation from '../../components/Navigation';
import axios from 'axios';

export default function index() {
	const router = useRouter();
	const [title, setTitle] = useState('');
	const [content, setContent] = useState('');
	const [postId, setPostId] = useState('');
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		if (router.query.id) {
			setPostId(router.query.id);
			axios.get(`${process.env.NEXT_PUBLIC_API_ENDPOINT}posts/${router.query.id}`).then((res) => {
				if (res.status === 200) {
					setTitle(res.data.title);
					setContent(res.data.content);
					setLoading(false);
				}
			});
		}
	}, [router.query]);

	const handleClickMenu = (value) => {
		if (value) {
			setChosenMenu(value);
			console.log('hitt', value);
		}
	};

	const handleClickSubmit = () => {
		if (title.length > 0 && content.length > 0) {
			const axiosConfig = {
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
				},
			};
			const payload = {
				title: title,
				content: content,
			};
			axios.put(`${process.env.NEXT_PUBLIC_API_ENDPOINT}posts/${postId}`, payload, axiosConfig).then((res) => {
				if (res.status === 200) {
					router.push({
						pathname: '/',
					});
				}
			});
		} else {
			alert('Judul maupun konten tidak boleh kosong');
		}
	};
	return (
		<Navigation handleClickMenu={handleClickMenu}>
			<Head>
				<title>Update Post</title>
				<meta name='viewport' content='initial-scale=1.0, width=device-width' />
			</Head>
			{loading ? (
				<div className='otoklix__loadingCenter' />
			) : (
				<>
					<h1 className='otoklix__mb30'>Update Your Post</h1>
					<div className='otoklix__flex otoklix__flexColumn'>
						<input
							placeholder='Enter the Title'
							type={'text'}
							id={'title'}
							autoComplete='off'
							value={title}
							onChange={(e) => setTitle(e.target.value)}
							className='otoklix__inputTitle otoklix__mb30'
						/>
						<textarea
							id='content'
							value={content}
							onChange={(e) => setContent(e.target.value)}
							placeholder='Write the content'
							className='otoklix__inputContent otoklix__mb30'
						/>
					</div>
					<button type='button' className='btn btn-outline-primary otoklix__submitButton' onClick={() => handleClickSubmit()}>
						Submit
					</button>
				</>
			)}
		</Navigation>
	);
}
