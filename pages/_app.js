import 'bootstrap/dist/css/bootstrap.min.css';
import '../assets/css/core.css';
import '../assets/css/sidebar.css';
import '../assets/css/card.css';

function MyApp({ Component, pageProps }) {
	return <Component {...pageProps} />;
}

export default MyApp;
