import Sidebar from '../components/Sidebar';

export default function Navigation({ children, chosenMenu, handleClickMenu }) {
	return (
		<div className='otoklix__heightFull row'>
			<div className='col-3 otoklix__sidebarBackground otoklix__pr0'>
				<Sidebar chosenMenu={chosenMenu} handleClickMenu={handleClickMenu} />
			</div>
			<div className='col'>{children}</div>
		</div>
	);
}
