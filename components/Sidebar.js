import Link from 'next/link';

export default function Sidebar({ chosenMenu, handleClickMenu }) {
	return (
		<>
			<Link href='/'>
				<div className='otoklix__titleSidebar otoklix__pointer'>Otoklix Sidebar</div>
			</Link>
			<div className='otoklix__menuSidebar otoklix__flex otoklix__flexColumn otoklix__flexAlignStart'>
				<Link href='/'>
					<div className={`${chosenMenu === 'posts' ? `otoklix__primaryText otoklix__activeButton` : `otoklix__secondaryText`} otoklix__pointer`}>
						<p className='otoklix__sidebarCustomText'>Posts</p>
					</div>
				</Link>
				<Link href='/new'>
					<div className={`${chosenMenu === 'newpost' ? `otoklix__primaryText otoklix__activeButton` : `otoklix__secondaryText`} otoklix__pointer`}>
						<p className='otoklix__sidebarCustomText'>Create New Post</p>
					</div>
				</Link>
			</div>
		</>
	);
}
