import { useEffect, useState } from 'react';
import Navigation from '../../components/Navigation';
import { useRouter } from 'next/router';
import Head from 'next/head';
import axios from 'axios';

export default function newPost() {
	const router = useRouter();
	const [chosenMenu, setChosenMenu] = useState('newpost');
	const [title, setTitle] = useState('');
	const [content, setContent] = useState('');

	const handleClickMenu = (value) => {
		if (value) {
			setChosenMenu(value);
			console.log('hitt', value);
		}
	};
	const handleClickSubmit = () => {
		if (title.length > 0 && content.length > 0) {
			const axiosConfig = {
				headers: {
					'Content-Type': 'application/json',
					Accept: 'application/json',
				},
			};
			const payload = {
				title: title,
				content: content,
			};
			axios
				.post(`${process.env.NEXT_PUBLIC_API_ENDPOINT}posts`, payload, axiosConfig)
				.then((res) => {
					if (res.status === 200) {
						router.push({
							pathname: '/',
						});
					}
				})
				.catch((err) => {
					console.log(err, 'err');
				});
		} else {
			if (content.length < 1 && title.length < 1) {
				alert('Mohon untuk mengisi Judul dan Konten.');
			} else if (content.length < 1) {
				alert('Mohon untuk mengisi Konten.');
			} else {
				alert('Mohon untuk mengisi Judul.');
			}
		}
	};
	return (
		<Navigation chosenMenu={chosenMenu} handleClickMenu={handleClickMenu}>
			<Head>
				<title>Create New Post</title>
				<meta name='viewport' content='initial-scale=1.0, width=device-width' />
			</Head>
			<div>
				<h1 className='otoklix__mb30'>Create New Post</h1>
				<div className='otoklix__flex otoklix__flexColumn'>
					<input
						placeholder='Enter the Title'
						type={'text'}
						id={'title'}
						autoComplete='off'
						value={title}
						onChange={(e) => setTitle(e.target.value)}
						className='otoklix__inputTitle otoklix__mb30'
					/>
					<textarea
						id='content'
						value={content}
						onChange={(e) => setContent(e.target.value)}
						placeholder='Write the content'
						className='otoklix__inputContent otoklix__mb30'
					/>
				</div>
				<button type='button' className='btn btn-outline-primary otoklix__submitButton' onClick={() => handleClickSubmit()}>
					Submit
				</button>
			</div>
		</Navigation>
	);
}
