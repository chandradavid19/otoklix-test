import { useEffect, useState } from 'react';
import axios from 'axios';
import Navigation from '../components/Navigation';
import Card from '../components/Card';
import Head from 'next/head';

export default function Home(props) {
	const [articles, setArticles] = useState([]);
	const [loading, setLoading] = useState(true);
	const [chosenMenu, setChosenMenu] = useState('posts');

	useEffect(() => {
		if (props) {
			setArticles(props.articles);
			setLoading(props.loading);
		}
	}, []);
	const handleGetPostData = () => {
		axios.get(`${process.env.NEXT_PUBLIC_API_ENDPOINT}posts`).then((res) => {
			if (res.status === 200) {
				setArticles(res.data);
				setLoading(false);
			}
		});
	};
	const handleClickMenu = (value) => {
		if (value) {
			setChosenMenu(value);
			console.log('hitt', value);
		}
	};

	const handleConfirmDelete = (id, title, content) => {
		const axiosConfig = {
			headers: {
				'Content-Type': 'application/json',
				Accept: 'application/json',
			},
		};
		const payload = {
			title: title,
			content: content,
		};
		axios.delete(`${process.env.NEXT_PUBLIC_API_ENDPOINT}posts/${id}`, payload, axiosConfig).then((res) => {
			if (res.status === 200) {
				setLoading(true);
				handleGetPostData();
			}
		});
	};
	const handleCancel = () => {
		console.log('cancel');
	};
	const handleDeletePost = (id, title, content) => {
		if (id) {
			window.confirm('Are you sure you wish to delete this item?') ? handleConfirmDelete(id, title, content) : handleCancel();
		}
	};

	return (
		<Navigation chosenMenu={chosenMenu} handleClickMenu={handleClickMenu}>
			<Head>
				<title>All Post</title>
				<meta name='viewport' content='initial-scale=1.0, width=device-width' />
			</Head>
			{loading ? (
				<div className='otoklix__loadingCenter' />
			) : (
				<div className='otoklix__flex otoklix__flexColumn'>
					{articles.length > 0 && (
						<>
							<h1 className='otoklix__mb30'>All Post</h1>
							<div className='otoklix__flex otoklix__flexWrap'>
								{articles.map((article, index) => {
									return (
										<div key={index}>
											<Card title={article.title} content={article.content} id={article.id} handleDeletePost={handleDeletePost} />
										</div>
									);
								})}
							</div>
						</>
					)}
				</div>
			)}
		</Navigation>
	);
}

export const getStaticProps = async () => {
	const res = await axios.get(`${process.env.NEXT_PUBLIC_API_ENDPOINT}posts`);
	return {
		props: {
			articles: res.data,
			loading: false,
		},
	};
};
